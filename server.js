const express = require("express")
const app = express()
const cookieParser = require("cookie-parser")
const i18n = require("i18n")
const path = require("path")

// Serving static files
app.use("/assets", express.static(path.join(__dirname, "public")))

app.set("port", 5000)
app.set("view engine", "ejs")

// We'll keep the language in a cookie
app.use(cookieParser())

i18n.configure({
    locales: ["en", "sr", "no"],
    cookie: "lang",
    directory: __dirname + "/locales"
})

// i18n middleware 
app.use(i18n.init)

/* Middleware to attach '__' property to res object
so we can use it in our templates */
app.use((req, res, next) => {
    res.locals.__ = res.__ = function() {
        return i18n.__.apply(req, arguments)
    }

    next()
})

app.get("/", (req, res) => {
    res.render("index")
})

app.get("*", (req, res) => {
    res.status(404).send("404")
})

app.listen(app.get("port"), () => {
    console.log(`App running on port ${app.get("port")}`)
})