// Basic check (find the better way to check it in production)
if (!document.cookie) {
    document.cookie = "lang=en";
}

// This will select each button in the document
var buttons = document.querySelectorAll("button");

// Attach event listeners to buttons
for (var i = 0; i < buttons.length; i++) {
    buttons[i].addEventListener("click", function(e) {
        document.cookie = "lang=" + e.target.className;
        location.reload();
    });
}


    
