# Express Localization

Basic example how to setup and use i18n localization module with your Express application.

![Image of Yaktocat](https://thumbs.gfycat.com/AdmirableDismalIcelandichorse-size_restricted.gif)
#### Dependencies

* [Express](https://github.com/expressjs/express)
* [EJS Template Engine](https://github.com/tj/ejs)
* [i18n Localization Module](https://github.com/mashpie/i18n-node)

#### Running

```sh
$ npm install
$ npm start
```
After that you should see: 

```sh
App running on port 5000
```